![UCV logo](https://www.ucv.edu.pe/wp-content/uploads/2020/05/ucv-logo.png)

<center><h2>Escuela de Ingeniería de Sistemas</h2> </center>
<center><br></p><br></p><h2>Proyecto de ANALISIS Y DISEÑO DE SISTEMAS</h2> </center>
<center><br></p><br></p><h2>Implementación de un sistema de registro y control de los dispositivos GPS para el área de logística de la empresa edsa asociados</h2> </center>
<br></p><br></p>
<h2>PROFESOR(es):

RICHARD LEONARDO BERROCAL NAVARRO(0000-0001-8718-3150)
</h2>
<br></p>
<h2>equipo Nro. 6</h2>
<center>

Participantes|Orcid|Participacion
---|---|---
Atuncar Meneses Bryan Alexander | 0000-0002-8637-7233|40%
Flores Cucho Katy Mirian | 0000|80%
Milian Hinostroza Willian Arturo | 0000-0002-8891-2009|40%
Ramos Felix Juan carlos | 0000|0%
Trinidad Salguero Gianfranco | 0000-0001-8925-8822|100%
Valdez Robles Victor Jesus | 0000-0003-4565-2099|100%

</center>
<center><h2><br></p>
Los Olivos, 2022
</h2> </center>
<br><br><br><br><br><br><br>
<center><br></p><br></p><h2>Implementación de un sistema de registro y control de los dispositivos GPS para el área de logística de la empresa edsa asociados</h2> <br><br></center>
RESUMEN EJECUTIVO <br>
Este proyecto esta orientado a la mejora de la gestion de datos de la empresa edsa asociados que actualmente solo cuenta con excel como unico gestor de informacion, el cual tiene fallas debido a su poca proteccion de integridad de la informacion que demoran los procesos debido a que se tiene que reconfirmar la informacion en ese cuadro constantemente.<br><br>
INTRODUCCIÓN<br>
La empresa esda asociados que fue creada en el año 2007 que se encarga de la venta y control de GPS para distintas entidades ,debido a la amplia informacion que maneja actualmente, la revision y control de esta se hace lenta y repetitiva ante esta situacion la propuesta planteada es la siguiente, implementar un sistema de control que cuente con una base de datos solida y estructurada que ayudara a reducir los tiempos de respuesta entre cliente y la empresa, ademas nos permitira tener una fuente de informacion accesible, confiable e integra.Lo que mejorara la atencion brindada y la seguridad de la informacion en la empresa.<br><br>

1. ESTUDIO DE FACTIBILIDAD<br><br>
El proyecto que busca mejorar la gestion de datos de la empresa edsa asociados S.A.C., se hace en base a las mejoras con sistemas vistas en los distintos sectores de la empresa, este proyecto se implementara unicamente en el area de logistica y al personal encargado de tratar con las ventas y consultas de los clientes, para este proyecto unicamente se necesitara el apoyo por parte del area de TI para la creacion de un usuario que tenga el acceso a la BD que usara el programa y tambien para la asistencia con la instalacion del programa en los equipos del personal indicado.<br><br>
1.1. Factibilidad operativa y técnica: La visión del sistema<br>
Desde el punto de vista operativo, el impacto del presente sistema de monitoreo y
Gestión de flota  son  innegable y sin grandes inconvenientes debido a los siguientes
Ítems:
La problemática es presentada por un cliente real que ya tiene un mercado establecido
En el tema de monitoreo  y que desea ampliar el alcance de su empresa permitiendo el
Desarrollo de la presente realizando grandes compras de equipo. Además, el cliente pretende mejorar su actual manera de manejar la información de su empresa la cual, como se mencionó anteriormente,
Carece de agilidad y rapidez al momento de ingresar datos y realizar consultar. Por lo tanto,
el sistema a exponer resuelve esta problemática y fija un punto de partida de la necesidad
planteada por el cliente.
 
El sistema será de fácil acceso, solo requerirá que trabajadores
Tengan un dispositivo monitor y el sistema instalado,
Al estar en constante contacto con el cliente mientras se desarrolló el software
Gracias al modelo de desarrollo seleccionado por el equipo de trabajo, permitió que el sistema
Se desarrolle a la medida y gustos del cliente, lo cual permite un menor tiempo de adaptación
y aprendizaje sobre el funcionamiento del sistema.
 Factibilidad Técnica
Para la realización del presente proyecto se necesitó la disposición de diferentes
Tecnologías, que son necesarias para implementar las funcionalidades.
En las primeras etapas del proyecto corresponden a estudio, análisis y diseño del
sistema a desarrollar por lo que ocuparan diferentes herramientas provenientes de Microsoft,
Característica de otorgar la posibilidad de programar tanto para IOS como Android, Firebase
la cual es una plataforma para diseño web y móvil la cual permite el uso de una base de datos no SQL y de tiempo real llamada “Realtime Database”, la cual presenta una gran ventaja al
Es momento de trabajar con posiciones y movimientos del GPS.
Para el desarrollo de la aplicación se utilizará un servidor local proveído por IONIC y una vez finalizado el software se procederá a subirla a una cuenta de Play Store con una cuenta brindada por el cliente mismo, por lo cual no necesita mayor análisis.


2. MODELO DEL NEGOCIO<br>
El enorme nivel de digitalización que ha alcanzado el mercado durante los últimos años ha permitido a las empresas apoyarse en soluciones que les permitan optimizar al máximo su rendimiento. En ese sentido, los sistemas GPS pueden ser una herramienta de carácter estratégico para las empresas  logística, especialmente en el actual contexto de pandemia, permitiendo agilizar sus operaciones y gestionar mejor los recursos.<br>
2.1. Modelo de Caso de Uso del Negocio<br>
El Modelo de Casos de Uso del Negocio (MCUN) o vista externa incluye la lista de los actores de negocio y el diagrama de los casos de uso del negocio


![Imagenpen](https://i.ibb.co/SX9QmPS/CASO-DE-USO.jpg)

![Imagenpen](https://i.picasion.com/pic92/7d5027e45d929363cd235863db566b45.gif)

![Imagenpen](https://i.ibb.co/QNSpPcS/sql.png)







NOMBRE| DESCRIPCION
---|---
Cliente|Es quien adquiere los servicios de un negocio
Socio|Persona que se une a otra para crear una empresa, teniendo en cuenta objetivos similares
Poveedores|Es una persona o una empresa que abastece a otras empresas con articulos
Propietarios|Personas que dominan la empresa


2.1.2. Lista de Casos de Usos del Negocio<br>

NOMBRE| DESCRIPCION
---|---
Cliente|Consumidor de productos o servicios de una empresa
Socio|Persona que participa del negocio junto con otras asociadas a la compañia
Poveedores|Proporciona bienes o servicios a otras personas o empresas
Propietarios|Personas que dominan la empresa


2.1.3. Diagrama de Casos de uso del Negocio<br>

Los diagramas de Casos de Uso del Negocio (DCUN) muestran las realciones de los AN con los CUN.
Pueden estar agrupados por paquete si el alcance así lo amerita.<br>

![Imagenpen](https://i.picasion.com/pic92/2d3b5002b4aeebbc0e0e133b161883c5.gif)


2.1.4. Especifiaciones de Casos de Usos del Negocio <br>

NOMBRE|DESCRIPCION
---|---
Descripcion|El sistema tiene dificultad al ingresar datos o al realizar alguna búsqueda, se hará lo posible para que el proceso de ingresar o buscar algún dato correspondiente sea de un mínimo tiempo para que resulte útil el trabajo.
Actores de negocio|Cliente, socio, proveedores, propietarios
Entradas|Sistema con agilidad en los procesos al ingresar datos 
Entregables|Se espera que con cada actor de negocio sea capaz de adquirir los resultados correspondientes a lo esperado
Mejoras|Se alojara un sistema operativo con el que sea capaz de al inresar datos al servidor este agilice el proceso

2.2.1. Lista de Trabajadores de Negocio

NOMBRE| DESCRIPCION
---|---
CLIENTE| Un cliente es quien realiza las compras de los gps 
SOCIO | Persona que se une a otra para crear una empresa, teniendo en cuenta objetivos similares.
PROVEEDORES | Es una persona o una empresa que abastece a otras empresas con artículos
PROPIETARIOS | Personas que dominan la empresa

2.2.2. Lista de Entidades de Negocio

NOMBRE| DESCRIPCION|ORIGEN|TIPO
---|---|---|---
Empresa Edsa Asociados |la empresa aprueba la compra de los equipos |I|F
Solicitud de Compra |La solicitud pasará por un proceso de aprobación mediante el cual se otorgará la autorización para comprar el artículo. |I|F
Orden de Compra |Se envía una orden de compra al proveedor. Aquí se identificarán los artículos que se adquieren, la cantidad requerida y el precio que se paga, así como los datos de la empresa. |I|P
Entrega de recibo|A continuación, el proveedor enviará la mercancía a la organización compradora. Una vez que las mercancías llegan, normalmente pasarán por algún tipo de proceso de recepción para garantizar que coincidan con lo que se pidió y que sean de la calidad correcta. |I|E
Factura de pago|En el momento del envío,la empresa  emitirá una factura, que acompaña a la mercancía o se envía por separado. Esto será recibido por el departamento de cuentas por pagar. |I|P


2.2.3. Realización de Casos de Uso del Negocio


2.3. Glosario de términos


NOMBRE| DESCRIPCION
---|---
Caso de uso| Describe el comportamiento de cómo un sistema responde a las solicitudes de uno de los involucrados relevantes llamado actor primario
CRUD| Tipo de funcionalidad en el desarrollo de un caso de uso (Create,Retrieve, Update, Delete)  (Crear,obtener,actualizar, eliminar)
estructura de caso de uso| elementos que deben identificarse en un documento de caso de uso para poder determinar correctamente los requerimientos funcionales de un sistema de software
extend| Define si un caso de uso se extiende de otro  se representa por medio de una linea o conector.
Flujo alternativo| El sistema comprueba la validez de los datos, si estos no son correctos, se avisa al actor de ello permitiéndole que los corrija.
Granularidad| Integrar de manera objetiva  la información basándose en objetos precisos reconocidos por el usuario para el efecto de descubrir  las relaciones que faciliten las interpretaciones  y toma de decisiones dentro del sistema  


2.4. Reglas de negocio

CODIGO|NOMBRE|DESCRIPCION|CASO DE USO AFECTADO
---|---|---|---
RNO1 |la regla 01 |un cliente solicita compra |Registrar Solicitud Compra
RNO2 |la regla 02 |para realizar la compra de equipos gps el cliente debe generar una orden |Orden de compra
RNO3 |la regla 03 |Para realizar una nueva compra el cliente podrá registrarse con su ruc  |Registrar Solicitud Compra
RNO4 |la regla 04 |Solo el gerente general Aprueba las las solicitudes de compra  |Registrar Solicitud Compra
 

3 . CAPTURA DE REQUERIMIENTOS<br>

Para poder realizar la implementación es necesario tener en cuenta el funcionamiento de esta empresa, y el trámite que se realiza luego de la venta de los equipos, por ende será necesario recabar información del personal que participa en el registro y control de los dispositivos GPS de los clientes de esta empresa<br>


3.1. Fuentes de obtención de requerimientos <br>

Para la obtención de la información se realizará una entrevista al personal de atención al cliente y los encargados del control de los dispositivos, además se coordinará con el encargado de área para saber como es que se presentará la información y qué campos son los que necesita en los informes que obtendrá del sistema.<br>

3.1.1. Informe de entrevistas o material técnico

REQUERIMIENTOS FUNCIONALES
•Debe permitirme generar planilla para los diferentes tipos de trabajadores.
•Debe mostrar un balance general de las ventas realizadas.
•Debe mostrar mediante estadísticas la ganancia semanal, mensual y anula de la empresa.
•Debe tener diferentes tipos de usuarios
.•Debe tener sistema de seguridad en contraseñas para el acceso.
•Debe generar reportes útiles.
•Debe tener modos de validación de datos
REQUERIMIENTOS
 NOFUNCIONALES
•Rendimiento
•Disponibilidad
•Seguridad
•Accesibilidad
•Usabilidad
•Estabilidad
•Portabilidad
•Costo
•Operatividad
INTERFACES
Hardware:
El sistema se debe implementar sobre las ventas de los  productos.
Software:
La aplicación deberá funcionar sobre SQL server.


3.1.2. Matriz de Actividades y Requerimientos<br>

ID|NOMBRE DEL REQUERIMIENTO|DESCRIPCION|ALCANCE, TIEMPO O COSTE|PRIORIDAD| PRUEBA DE VERIFICACION|CRITERIO DE ACEPTACION
---|---|---|---|---|---|---
RF-01|Inicio de sesión|El sistema debe permitir al encargado de almacen, asesor de ventas, coordinador de área,encargado de ventas y el encargado de atención al usuario iniciar sesión con usuario y contraseña personal e ingresar al menú principal.|TIEMPO|Media|ACTAS DE REUNION|DISEÑO QUE CUMPLA CPON EL 100% DE LAS ESPECIFICACIONES DEL CASO|
RF-02|Buscar rutas |El sistema debe permitir al encargado de venta buscar las rutas registradas en la base de datos.|TIEMPO|Alta|ACTAS DE REUNION|DISEÑO QUE CUMPLA CPON EL 100% DE LAS ESPECIFICACIONES DEL CASO|
RF-03|Consultar historial de rutas|El sistema debe permitir al coordinador consultar el historial de rutas registrados por encargado de monitoreo.|TIEMPO|Alta|ACTAS DE REUNION|DISEÑO QUE CUMPLA CPON EL 100% DE LAS ESPECIFICACIONES DEL CASO|
RF-04|Consultar información Clientes|El sistema debe permitir al encargado de venta consultar información de los clientes|TIEMPO|Alta|ACTAS DE REUNION|DISEÑO QUE CUMPLA CPON EL 100% DE LAS ESPECIFICACIONES DEL CASO|
RF-05|Registrar nuevo pedido de cliente|El sistema debe permitir al encargado de venta registrar un pedido del cliente.|TIEMPO|Alta|ACTAS DE REUNION|DISEÑO QUE CUMPLA CPON EL 100% DE LAS ESPECIFICACIONES DEL CASO|
RF-06|Mantenimiento de rutas|El sistema debe permitir al jefe de almacén hacer el mantenimiento previo de las rutas|TIEMPO|Alta|ACTAS DE REUNION|DISEÑO QUE CUMPLA CPON EL 100% DE LAS ESPECIFICACIONES DEL CASO|
RNF-07|Se usará el software C#|El sistema deberá tener el software C#|ALCANCE|Alta|ACTAS DE REUNION|PROTOTIPO DEBE CUMPLIR CON REQUERIMIENTOS DE SOFTWARE|
RNF-08|Se usará SQL server|El sistema deberá ser implementado  en SQL server para la base de datos.|ALCANCE|Alta|ACTAS DE REUNION|PROTOTIPO DEBE CUMPLIR CON REQUERIMIENTOS DE SOFTWARE|



3.2. Modelo de Casos de Uso<br>


![Imagenpen](https://i.ibb.co/mzHBHfs/LOGIN.png)

3.2.1. Lista de Actores del Sistema<br>

NOMBRE| DESCRIPCION
---|---
Encargado del almacen|Se encarga del registro y control de inventario
Asesor de ventas|Se encarga del registro de ventas
Coordinador de área|Se encarga de realizar los informes de acuerdo a la información obtenida del registro de ventas
Encargado de ventas|Realiza control de las ganancias de las ventas y las mensualidades por el servicio de monitoreo de los dispositivos
Encargado de atención al usuario|Se encarga de atender las solicitudes de ubicación de los dispositivos de los clientes


3.2.2. Lista de Casos de Uso del Sistema<br>

NOMBRE| DESCRIPCION
---|---
Ingreso y salida de los dispositivos|El área de almacén se encarga de llevar un control de los dispositivos
Monitoreo de dispositivos GPS|Se corrobora de que el dispositivo esté operando con normalidad
Venta|Cuando se realiza una venta se ingresa la información al sistema sobre el dispositivo vendido, sobre el cliente, sobre el plazo de monitoreo y el precio del dispositivo.


3.2.3. Lista de Casos de Uso priorizados

ACTOR|CASO DE USO|COMPLEJIDAD|PROCEDENCIA|PREMURA|RIESGO|TOTAL
---|---|---|---|---|---|---
Encargado del sistema|UBICACIÓN DE LA UNIDAD MÓVIL|0.5|0.2|0.2|0.1|1
Personal operativo de sistemas|REGISTRO O ELIMINACIÓN DE UNIDADES|0.4|0.3|0.2|0.1|1
Personal operativo de sistemas|GENERAR REPORTES DIARIOS|0.5|0.2|0.2|0.1|1


3.2.4. Diagramas de Caso de Uso del Sistema<br>
![Imagenpen](https://i.ibb.co/mzHBHfs/LOGIN.png)
![Imagenpen](https://i.ibb.co/MBCmRqy/REGISTRA-O-ELIMINA-UNIDAD.png)
![Imagenpen](https://i.ibb.co/HGFK9yp/REPORTE-DE-RUA.png)
![Imagenpen](https://i.ibb.co/5h7Q2wh/DESPACHO-2.png)
![Imagenpen](https://i.ibb.co/hfSWdD8/DESPACHO.png)
![Imagenpen](https://i.ibb.co/ynyPpgz/GENERA-CODIGO.png)



3.2.5. Especificaciones de Requerimientos de Software<br>

FORMULARIO| AUTENTICAR USUARIO
---|---
Nombre|AUTENTICAR USUARIO
Usuario|Administrador, supervisor de operaciones y secretaria.
Prioridad en negocio|Bajo
Riesgo en desarrollo|Bajo
Iteracion Asignada|1
Descripción|El usuario para acceder al sistema ingresará su usuario y contraseña. El sistema realizará una validación si los datos son correctos y mostrará la vista del menú principal. Así mismo podrá cerrar su sesión cuando lo desee seleccionando la opción salir
Criterios de aceptación|Ingreso de usuario correcto, ingreso de usuario incorrecto y cerrar sesión del sistema.


FORMULARIO| BUSQUEDA DE CLIENTE
---|---
Nombre|BUSQUEDA DE CLIENTE
Usuario|Administrador, supervisor de operaciones y secretaria.
Prioridad en negocio|Bajo
Riesgo en desarrollo|Medio
Iteracion Asignada|2
Descripción|El usuario necesita tener el RUC del cliente para validar la informacion dada, tales como nombre y ubicacion
Criterios de aceptación|el RUC es incorrecto, El RUC es correcto


FORMULARIO| OPERACION
---|---
Nombre|OPERACION
Usuario|Administrador, supervisor de operaciones y secretaria.
Prioridad en negocio|Bajo
Riesgo en desarrollo|Bajo
Iteracion Asignada|2
Descripción|El usuario necesita indicar el proceso a realizar VENTA o SUSCRIPCION
Criterios de aceptación|Proceso seleccionado

FORMULARIO| VENTAS
---|---
Nombre|VENTAS
Usuario|Administrador, supervisor de operaciones y secretaria.
Prioridad en negocio|Bajo
Riesgo en desarrollo|Bajo
Iteracion Asignada|2
Descripción|El usuario necesita registrar todos los datos del producto vendido
Criterios de aceptación|venta completada, serie incorrecta, serie correcta

FORMULARIO| SUSCRIPCION
---|---
Nombre|SUSCRIPCION
Usuario|Administrador, supervisor de operaciones y secretaria.
Prioridad en negocio|Bajo
Riesgo en desarrollo|Bajo
Iteracion Asignada|1
Descripción|El usuario necesita indicar el tiempo de duracion de la suscripcion
Criterios de aceptación|suscripcion completa, suscripcion vigente hasta fecha

3.2.6. Especificaciones de Casos de Uso<br>


CUSO01: UBICACIÓN DE LA UNIDAD MÓVIL: el encargado de monitoreo debe rastrear la unidad y verificar el recorrido de la unidad hasta llegar al destino, grabar los datos registrados por la unidad en seguimiento por el GPS.<br>


CUSO02: REGISTRO O ELIMINACIÓN DE UNIDADES: el encargado de sistemas crea un mecanismo que permita hacer el registro o eliminación de unidades y clientes.<br>


CUSO03: GENERAR REPORTES DIARIOS: el sistema debe contar con un reporte de seguimiento diario con respaldo de la información.<br>

CUSO01| Ubicacion de la unidad
---|---
Tipo|Crear un mecanismo que permita ubicar  la unidad móvil terrestre
Autor|encargado de monitoreo
Actores|Personal de sistemas
Casos de uso relacionados|interfaz de ubicación en el sector
Breve Descripción|El caso de uso permite ubicar  la unidad móvil terrestre, la distancia recorrida.
Referencias|Cancelar la operación, Operación completada e Información incompleta
Pre condiciones|La unidad móvil debe haberse registrado en la plataforma ,La unidad móvil debe tener el hardware colocado ,El usuario debe aportar su ubicación para que el sistema permita mostrar las rutas o alimentadores cerca
Post condiciones|La información puede ser vista detalladamente



CUSO02| REGISTRO O ELIMINACIÓN DE UNIDADES
---|---
Tipo|Crear un mecanismo que permita registrar o eliminar un dispositivo de la unidad móvil.
Autor|Personal operativo de sistemas
Actores| Personal operativo de sistemas
Breve Descripción|El caso de uso permite registrar o eliminar un dispositivo de la unidad móvil.
Referencias|Registro exitoso y Dispositivo eliminado
Pre condiciones|El dispositivo tiene que estar con conexión de internet,La unidad móvil debe haberse registrado en la plataforma y La unidad móvil debe tener el hardware colocado
Post condiciones|Verificar información de registro


CUSO03| GENERAR REPORTE DIARIO
---|---
Tipo|El sistema deberá generar un reporte de flujo diario
Autor|Personal operativo de sistemas
Actores|Personal operativo de sistemas
Breve Descripción|El caso de uso permite generar reportes diarios o semanales para el monitoreo y control
Referencias|Registro de clientes
Pre condiciones|El sistema debe validar los datos de los clientes
Post condiciones|Verificar información de reporte


3.7 Diagrama de estado


![Imagenasdasn](https://i.ibb.co/8KccFjK/estad.png)


CONCLUSIONES:
La tecnología del GPS mejora continuamente haciendo uso de la red de datos
de telefonía celular, para poder visualizar, entregar, controlar, etc., cada pedido,
esto nos determina, que esta tecnología en el corto plazo se convertirá en el
referente de control y optimización de procesos logísticos de manera eficiente,
así como brindar un adecuado control patrimonial de los activos en tránsito,
también determinará la reducción de accidentabilidad en el transporte terrestre.
La utilización de GPS no está muy difundida en el Perú, como también la no
explotación correcta del dispositivo, siendo este un costo más en las empresas
por exigencia del seguro contratado; sin embargo, Freesatel está enfocado a
revertir este concepto, para ello brindara un servicio adecuado con la finalidad
de reducir costos significativos en las operaciones
Con LA UTILIZACION DE  GPS, se podría determinar las rutas de transporte y medir si una ruta es rentable o esta sobrecargada. Esto actualmente se ve que todas las rutas de transporte publico están saturadas y se pierde mucho tiempo en sus
desplazamientos

